React-Redux boilerplate
=======================

Basic React/ES6/Webpack project that I intend to use for learning the Redux
ecosystem.

# Start application

```
npm start
```

# Browse application

Open http://localhost:3400/

Please note, you will need a modern, ES6-capable browser to run the application
as the Babel transpiler has been configured to only manage CommonJS modules and
the rest of JavaScript code is bundled in its original, ES6 form. You might
want to use the most recent versions of Google Chrome, Mozilla Firefox and
Opera as well as Safari Techonology Preview to use the application.

If you wish to customise the port number, edit the relevant entry in
`package.json` or execute the following command:

```
npm config set redux-boilerplate:port <your port number>
```

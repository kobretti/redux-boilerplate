var path = require('path')
var packageConfig = require('../package.json')

var rootDir = path.join(__dirname, '../')

var config = {
  context: rootDir,
  entry: [
    packageConfig.main
  ],
  output: {
    path: path.join(rootDir, 'dist/js'),
    filename: 'bundle.min.js',
    publicPath: '/dist/'
  },
  module: {
    loaders: [{
      test: /\.jsx?/,
      exclude: /node\_modules/,
      loaders: ['babel']
    }]
  },
  resolve: {
    extensions: ['', '.js', '.jsx']
  }
}

module.exports = config

var path = require('path')
var webpack = require('webpack')
var mergeWebpackConfig = require('webpack-merge')

var rootDir = path.join(__dirname, '../')

var commonConfig = require('./webpack.config.base')

var config = {
  entry: [
    'webpack-dev-server/client?http://localhost:' +
      process.env.npm_package_config_port,
    'webpack/hot/only-dev-server',
    process.env.npm_package_main
  ],
  output: {
    path: path.join(rootDir, 'dist/js'),
    filename: 'bundle.min.js',
    publicPath: '/dist/js/'
  },
  module: {
    loaders: [{
      test: /\.jsx?/,
      loaders: ['react-hot', 'babel'],
      include: path.join(rootDir, './src')
    }, {
      test: /\.css$/,
      loaders: ['style', 'css'],
      include: path.join(rootDir, './src')
    }]
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin()
  ],
  devtool: 'source-map'
}

module.exports = mergeWebpackConfig(commonConfig, config)

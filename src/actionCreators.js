import { CLICK, VALIDATE } from './actions'

export function navigateTo(route) {
  return { type: CLICK, route }
}

export function validateField(field, value) {
  return {
    type: VALIDATE,
    key: field,
    value
  }
}

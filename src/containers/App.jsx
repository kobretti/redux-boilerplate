import React, { PropTypes } from 'react'
import { connect } from 'react-redux'
import { navigateTo } from '../actionCreators'
import { requestAsyncData } from '../requestAsyncData'

class App extends React.Component {

  render () {
    const props = this.props

    const message = 'Hello, world!'
    return (<div>
      <h1>{message}</h1>
      { console.log(props.fullState) }
      <p>
        <button onClick={props.fetchData}>Fetch data asynchronously</button>
      </p>
      <fieldset>
        <button onClick={props.navigateBackward}>Previous view</button>
        <button onClick={props.navigateForward}>Next view</button>
      </fieldset>
    </div>)
  }
}

const mapStateToProps = (state) => {
  return {
    fullState: state
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchData: () => dispatch(requestAsyncData()),
    // A better place for the navigation concern?
    navigateBackward: () => dispatch(navigateTo(-1)),
    navigateForward: () => dispatch(navigateTo(1))
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App)

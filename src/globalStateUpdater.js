import { combineReducers } from 'redux'

import { updateClonedClickState } from './updateClonedClickState'
import { updateClonedContactDetailsState } from './updateClonedContactDetailsState'
import { updateClonedValidationState } from './updateClonedValidationState'

export default combineReducers({
  navigationHistory: updateClonedClickState,
  contactDetails: updateClonedContactDetailsState,
  validation: updateClonedValidationState
})

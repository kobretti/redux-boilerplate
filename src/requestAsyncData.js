import fetch from 'axios'
import { DATA_REQUEST_OK } from './actions'

function createDataReadyAction(data) {
  return {
    type: DATA_REQUEST_OK,
    payload: data
  }
}

export function requestAsyncData() {
  return function(dispatch) {
    return fetch('http://localhost:3000/ContactDetails')
      .then(function (response) {
        dispatch(createDataReadyAction(response.data))
      })
      .catch(function (response) {
        console.error('requestAsyncData', response)
      })
  }
}


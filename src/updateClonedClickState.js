import {CLICK} from './actions'

const initialState = []

// Reducer in Redux speak
export function updateClonedClickState(state = initialState, action) {
  if (action.type === CLICK) {
    return [
      ...state,
      action.route
    ]
  }
  return state
}

import { DATA_REQUEST_OK } from './actions'

const initialState = {
  billing: {
    personTitle: '',
    firstName: '',
    lastName: ''
  },
  delivery: {
    personTitle: '',
    firstName: '',
    lastName: ''
  }
}

// Reducer in Redux speak
export function updateClonedContactDetailsState(state = initialState, action) {
  if (action.type === DATA_REQUEST_OK) {
    return Object.assign({}, state, action.payload)
  }
  return state
}

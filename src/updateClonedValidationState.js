import {VALIDATE} from './actions'

const initialState = {
  validatedFields: [],
  validFields: [],
  invalidFields: []
}

// Reducer in Redux speak
export function updateClonedValidationState(state = initialState, action) {
  if (action.type === VALIDATE) {
    state.validatedFields = [...state.validatedFields, action.key]
    if (isValid(action.key, action.value)) {
      state.validFields = [...state.validFields, action.key]
    } else {
      state.invalidFields = [...state.invalidFields, action.key]
    }
  }
  return state
}

function isValid(key, value) {
  if (key === 'firstName') {
    return true
  }
  if (key === 'lastName') {
    return false
  }
  return null
}

var npmCommand = process.env.npm_lifecycle_event
var environment = npmCommand.split(':')[0]

module.exports = require('./config/webpack.config.' + environment)
